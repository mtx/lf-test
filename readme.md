## Misc Notes
- fonts in pt/px?
- print version
- responsive? (minimally)
  - header (1-3 / 2)
  - Explorare
- see also: [css styleguide/classes](assets/styleguide.html) 

## Tasks

+ ~git init~
+ ~styleguide: get fonts & colours~
+ ~CSS components~
+ ~HTML/CSS~
+ ~deploy~
  + ~vcs~
  + ~now / zeit~

## Known Bugs

- visually aproximated sizing / spacing
- inconsistent naming convention
- mixed bootstrap w generic components
- Roboto Slab @ Google FOnts doesn't do diacritice?!
- Table Aplicații / 1 – doesn't render nice in print – could potentially fare better w 'flex'
- only tested in Chrome & FF / MacOS

### Enhancements / Nice to Haves

- templating system
- styleguide + css components
- min css
- remove unneeded boostrap

---

Desktop

![desktop](assets/screenshots/desktop-Screen-Shot-2019-12-29-at-17.05.16.png "dekstop")

Mobile

![mobile](assets/screenshots/mobile-Screen-Shot-2019-12-29-at-17.06.19.png "mobile")

Mobile bug

![mobile bug](assets/screenshots/mobile-bug-Screen-Shot-2019-12-29-at-17.06.30.png "mobile bug")
